package com.testapp.weathermobile.rest.models;

import com.google.gson.annotations.SerializedName;

import retrofit.RetrofitError;

/**
 * Created by vverbitskiy on 4/26/15.
 */


public class Error {

    private int cod;
    private String message;

    public Error(int code, String message) {
        this.cod = code;
        this.message = message;
    }

    public String getError() {
        return message;
    }

}
