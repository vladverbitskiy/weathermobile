package com.testapp.weathermobile.dao;

/**
 * Created by vverbitskiy on 4/24/15.
 */
public class Coordinates {

    private double lon;
    private double lat;

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
