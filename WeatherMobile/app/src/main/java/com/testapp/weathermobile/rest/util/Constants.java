package com.testapp.weathermobile.rest.util;

/**
 * Created by vverbitskiy on 4/24/15.
 */
public class Constants {

    public static final String BASE_URL = "http://api.openweathermap.org";
    public static final String PATH_WEATHER = "/weather";
    public static final String PATH_DATA = "/data/2.5";
    public static final String PATH_IMAGE = "/img/w";

}
