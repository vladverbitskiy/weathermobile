package com.testapp.weathermobile.dao;

/**
 * Created by vverbitskiy on 4/24/15.
 */
public class Wind {

    private double speed;
    private double deg;

    public double getSpeed() {
        return speed;
    }

    public double getDeg() {
        return deg;
    }
}
