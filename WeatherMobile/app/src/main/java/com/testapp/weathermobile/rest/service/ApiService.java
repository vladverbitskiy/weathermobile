package com.testapp.weathermobile.rest.service;

import com.testapp.weathermobile.dao.WeatherObject;
import com.testapp.weathermobile.rest.ClientCallback;
import com.testapp.weathermobile.rest.util.Constants;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by vverbitskiy on 4/24/15.
 */
public interface ApiService {

    @GET(Constants.PATH_DATA + Constants.PATH_WEATHER + "/")
    public void getWeather(@Query("q") String queryValue, ClientCallback<WeatherObject> callback);

}
