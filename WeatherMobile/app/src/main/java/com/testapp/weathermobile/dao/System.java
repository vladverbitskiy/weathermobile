package com.testapp.weathermobile.dao;

/**
 * Created by vverbitskiy on 4/24/15.
 */
public class System {

    private double message;
    private String country;
    private long sunrise;
    private long sunset;

    public double getMessage() {
        return message;
    }

    public String getCountry() {
        return country;
    }

    public long getSunrise() {
        return sunrise;
    }

    public long getSunset() {
        return sunset;
    }
}
