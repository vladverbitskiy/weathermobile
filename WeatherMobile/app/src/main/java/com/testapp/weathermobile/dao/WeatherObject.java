package com.testapp.weathermobile.dao;

import java.util.ArrayList;

/**
 * Created by vverbitskiy on 4/24/15.
 */
public class WeatherObject {

    private Coordinates coord;
    private System sys;
    private ArrayList<Weather> weather;
    private String base;
    private Main main;
    private Wind wind;
    private Cloud clouds;
    private long dt;
    private int id;
    private String name;
    private int cod;

    public Coordinates getCoord() {
        return coord;
    }

    public System getSys() {
        return sys;
    }

    public Weather getWeather() {
        return weather.get(0);
    }

    public String getBase() {
        return base;
    }

    public Main getMain() {
        return main;
    }

    public Wind getWind() {
        return wind;
    }

    public Cloud getClouds() {
        return clouds;
    }

    public long getDt() {
        return dt;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getCod() {
        return cod;
    }

    @Override
    public String toString() {
        return "WeatherObject{" +
                "coord=" + coord +
                ", sys=" + sys +
                ", weather=" + weather +
                ", base='" + base + '\'' +
                ", main=" + main +
                ", wind=" + wind +
                ", clouds=" + clouds +
                ", dt=" + dt +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", cod=" + cod +
                '}';
    }
}
