package com.testapp.weathermobile.rest;

import com.google.gson.Gson;
import com.testapp.weathermobile.rest.models.Error;
import com.testapp.weathermobile.rest.util.ResponseParser;

import retrofit.Callback;
import retrofit.client.Response;

/**
 * Created by vverbitskiy on 4/24/15.
 */
public abstract class ClientCallback<T> implements Callback<T> {


    public abstract void failure(com.testapp.weathermobile.rest.models.Error error);
    public abstract void success(T data);

    @Override
    public void success(T t, Response response) {
        String r = ResponseParser.parseResponse(response);
        Error callbackError = new Gson().fromJson(r, Error.class);

        if (response.getStatus() == 200) {
            if (callbackError.getError() == null) {
                success(t);
            } else {
                failure(callbackError);
            }
        }
    }
}
