package com.testapp.weathermobile.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by vverbitskiy on 4/26/15.
 */
public class WeatherDataConverter {

    public static String getFormattedTime(long sourceData) {
        SimpleDateFormat sourceFormat = new SimpleDateFormat("ss");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        SimpleDateFormat needfullFormat = new SimpleDateFormat("HH:mm");

        Date source = new Date();

        try {
            source = sourceFormat.parse(String.valueOf(sourceData));
            return needfullFormat.format(source);
        } catch (ParseException pe) {
            pe.printStackTrace();
        }

        return null;
    }

    public static String getCelsiusValue(double kelvinValue) {
        return String.valueOf(Math.round(kelvinValue - 273.15));
    }

    public static String getMMHGValue(double hpaValue) {
        return String.valueOf(Math.round(hpaValue * 0.75006375541921));
    }

}
