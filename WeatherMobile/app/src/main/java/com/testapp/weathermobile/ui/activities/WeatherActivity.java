package com.testapp.weathermobile.ui.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.testapp.weathermobile.R;
import com.testapp.weathermobile.dao.WeatherObject;
import com.testapp.weathermobile.rest.ClientCallback;
import com.testapp.weathermobile.rest.RestClient;
import com.testapp.weathermobile.rest.models.*;
import com.testapp.weathermobile.rest.util.Constants;
import com.testapp.weathermobile.utils.WeatherDataConverter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

@EActivity(R.layout.activity_splash)
public class WeatherActivity extends Activity {

    private static final String LOG_TAG = WeatherActivity.class.getSimpleName();

    private static final String BUNDLE_WEATHER = "com.testapp.weathermobile.weather_object";
    private static final String BUNDLE_SEARCH_QUERY = "com.testapp.weathermobile.search_query";

    protected GoogleMap mGoogleMap;

    private MapFragment mGoogleMapFragment;
    private ScrollView mWeatherInfoView;

    protected WeatherObject mCurrentWeatherObject;

    private SearchView mSearchView;

    @ViewById(R.id.weatherInfoImage)
    protected ImageView mWeatherImage;
    @ViewById(R.id.cityName)
    protected TextView mCityName;
    @ViewById(R.id.sunriseValue)
    protected TextView mSunriseValue;
    @ViewById(R.id.sunsetValue)
    protected TextView mSunsetValue;
    @ViewById(R.id.temperatureValue)
    protected TextView mTemperatureValue;
    @ViewById(R.id.humidityValue)
    protected TextView mHumidityValue;
    @ViewById(R.id.tempMaxValue)
    protected TextView mTempMaxValue;
    @ViewById(R.id.tempMinValue)
    protected TextView mTempMinValue;
    @ViewById(R.id.pressureValue)
    protected TextView mPressureValue;
    @ViewById(R.id.seaLevelValue)
    protected TextView mSeaLevelValue;
    @ViewById(R.id.groundLevelValue)
    protected TextView mGroundLevelValue;
    @ViewById(R.id.speedValue)
    protected TextView mWindSpeedValue;
    @ViewById(R.id.degreeValue)
    protected TextView mWindDegreeValue;
    @ViewById(R.id.weatherMainValue)
    protected TextView mWeatherMainValue;
    @ViewById(R.id.descriptionMainValue)
    protected TextView mWeatherDescriptionValue;
    @ViewById(R.id.cloudsValue)
    protected TextView mCloudsValue;

    public ProgressDialog mProgressDialog;

    private String mSavedSearchString;

    @AfterViews
    protected void afterViews() {
        configureMap();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.weather_activity_actions, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        mSearchView.setQueryHint(getResources().getString(R.string.srt_search_hint));
        mSearchView.setSubmitButtonEnabled(true);

        if (mSavedSearchString != null) {

            if (!TextUtils.isEmpty(mSavedSearchString)) {
                searchItem.expandActionView();
                mSearchView.setQuery(mSavedSearchString, true);
                mSearchView.clearFocus();

            }
        }

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                getWeather(query);
                mSearchView.clearFocus();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mCurrentWeatherObject != null) {
            outState.putString(BUNDLE_WEATHER, new Gson().toJson(mCurrentWeatherObject));
        }

        outState.putString(BUNDLE_SEARCH_QUERY, mSearchView.getQuery().toString());
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState.containsKey(BUNDLE_WEATHER)) {
            mCurrentWeatherObject = new Gson().fromJson(savedInstanceState.getString(BUNDLE_WEATHER), WeatherObject.class);

            mGoogleMap = mGoogleMapFragment.getMap();

            focusMap(mCurrentWeatherObject);
            fillInformationViews(mCurrentWeatherObject);
        }

        if (savedInstanceState.containsKey(BUNDLE_SEARCH_QUERY)) {
            mSavedSearchString = savedInstanceState.getString(BUNDLE_SEARCH_QUERY);
        }

    }

    private void fillInformationViews(WeatherObject weather) {
        Picasso.with(WeatherActivity.this).load(Constants.BASE_URL + Constants.PATH_IMAGE + "/" + weather.getWeather().getIcon() + ".png").into(mWeatherImage);

        mCityName.setText(weather.getName() + ", " + weather.getSys().getCountry());
        mSunriseValue.setText(WeatherDataConverter.getFormattedTime(weather.getSys().getSunrise()));
        mSunsetValue.setText(WeatherDataConverter.getFormattedTime(weather.getSys().getSunset()));
        mTemperatureValue.setText(WeatherDataConverter.getCelsiusValue(weather.getMain().getTemp()) + " \u2103");
        mHumidityValue.setText(String.valueOf(weather.getMain().getHumidity()) + " \u0025");
        mTempMaxValue.setText(WeatherDataConverter.getCelsiusValue(weather.getMain().getTempMax()) + " \u2103");
        mTempMinValue.setText(WeatherDataConverter.getCelsiusValue(weather.getMain().getTempMin()) + " \u2103");
        mPressureValue.setText(WeatherDataConverter.getMMHGValue(weather.getMain().getPressure()) + " mmHg");
        mSeaLevelValue.setText(WeatherDataConverter.getMMHGValue(weather.getMain().getSeaLevel()) + " mmHg");
        mGroundLevelValue.setText(WeatherDataConverter.getMMHGValue(weather.getMain().getGrndLevel()) + " mmHg");
        mWindSpeedValue.setText(String.valueOf(weather.getWind().getSpeed()) + " mps");
        mWindDegreeValue.setText(String.valueOf(weather.getWind().getDeg()));
        mWeatherMainValue.setText(String.valueOf(weather.getWeather().getMain()));
        mWeatherDescriptionValue.setText(String.valueOf(weather.getWeather().getDescription()));
        mCloudsValue.setText(String.valueOf(weather.getClouds().getAll()) + " \u0025");
    }

    private void getWeather(String city) {
        showProgressDialog();
        RestClient.getInstance().getApiService().getWeather(city, new ClientCallback<WeatherObject>() {

            @Override
            public void failure(com.testapp.weathermobile.rest.models.Error error) {
                hideProgressDialog();

                Toast.makeText(WeatherActivity.this, error.getError(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void success(WeatherObject weatherObject) {
                hideProgressDialog();

                mCurrentWeatherObject = weatherObject;

                focusMap(weatherObject);
                fillInformationViews(weatherObject);
            }

            @Override
            public void failure(RetrofitError error) {
                hideProgressDialog();

                if (error.getKind() == RetrofitError.Kind.NETWORK) {
                    Toast.makeText(WeatherActivity.this, getResources().getString(R.string.str_network_error), Toast.LENGTH_SHORT).show();
                    Log.e(LOG_TAG, " [ WeatherRequest ] -> network error ");
                }

                if (error.getKind() == RetrofitError.Kind.HTTP) {
                    Toast.makeText(WeatherActivity.this, getResources().getString(R.string.str_server_error), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void configureMap() {
        mGoogleMapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map_fragment);
        mGoogleMapFragment.getMapAsync(new MapReadyCallback());
    }

    private void focusMap(WeatherObject weather) {

        if (mGoogleMap != null) {

            LatLng currentPointer = new LatLng(weather.getCoord().getLat(), weather.getCoord().getLon());

            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentPointer, 10.0f));

            mGoogleMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(android.R.drawable.arrow_down_float))
                    .position(currentPointer));

            CameraPosition cameraPosition = CameraPosition.builder()
                    .target(currentPointer)
                    .zoom(10.0f)
                    .build();

            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 5000, null);

        }
    }

    private class MapReadyCallback implements OnMapReadyCallback {

        @Override
        public void onMapReady(GoogleMap googleMap) {
            mGoogleMap = googleMap;
        }
    }

    private void showProgressDialog() {
        mProgressDialog = ProgressDialog.show(WeatherActivity.this, null, getResources().getString(R.string.str_loading_data), false, false);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    private void hideProgressDialog() {
        mProgressDialog.dismiss();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }
}
