package com.testapp.weathermobile.rest;

import com.testapp.weathermobile.rest.service.ApiService;
import com.testapp.weathermobile.rest.util.Constants;

import retrofit.RestAdapter;

/**
 * Created by vverbitskiy on 4/24/15.
 */
public class RestClient {

    private static RestClient instance;
    private ApiService mApiService;

    public static RestClient getInstance() {
        if (instance == null) {
            instance = new RestClient();
        }
        return instance;
    }

    public RestClient() {

        RestAdapter adapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Constants.BASE_URL)
                .build();

        mApiService = adapter.create(ApiService.class);

    }

    public ApiService getApiService() {
        return mApiService;
    }

}
