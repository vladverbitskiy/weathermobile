package com.testapp.weathermobile.dao;

/**
 * Created by vverbitskiy on 4/24/15.
 */
public class Main {

    private double temp;
    private double temp_min;
    private double temp_max;
    private double pressure;
    private double sea_level;
    private double grnd_level;
    private double humidity;

    public double getTemp() {
        return temp;
    }

    public double getTempMin() {
        return temp_min;
    }

    public double getTempMax() {
        return temp_max;
    }

    public double getPressure() {
        return pressure;
    }

    public double getSeaLevel() {
        return sea_level;
    }

    public double getGrndLevel() {
        return grnd_level;
    }

    public double getHumidity() {
        return humidity;
    }
}
